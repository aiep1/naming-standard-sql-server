![](RackMultipart20200506-4-vgmn5t_html_bfce161913ec01f5.png)

# Nomenclaturas

**Módulo:** Taller Base de Datos.

**Profesor:** Bernardo Lisboa Flores.

**Participantes:**

Ramírez Rivera Henrry David.

Garrido Osorio Andrea Andrés.

Garay Antillanca Efraín Hipólito.

_Santiago, mayo 2020._

**Nomenclaturas.**

Reglas generales para nombrar objetos de base de datos:

1. No se deben nombrar con espacios.
2. Idealmente 30 caracteres o menos, pensando en futuras portabilidades a otros motores de base de datos.
3. Debe ser los más descriptivo posible.
4. Los nombres no deben estar ligados al software o hardware utilizado.
5. No usar palabras reservadas.

| **Objeto** | **Notación** | **Longitud Max** | **Plural** | **Prefijo** | **Sufijo** | **Macara** | **Ejemplo** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Tabla | UPPER\_CamelCase | 30 | No | Nombre de proyecto abreviado | No | [A-Z]\_[A-z] | VTA\_ClienteSap |
| Vista | UPPER\_CamelCase | 30 | No | VIEW | No | [A-Z]\_[A-z] | VIEW\_ClienteComuna |
| Procedimiento | UPPER\_CamelCase\_UPPER | 30 | No | Nombre de proyecto abreviado | SP | [A-Z]\_[A-z]\_SP | VTA\_CrearUsaurio\_SP |
| Funciones | CamelCase\_UPPER | 30 | No | No | U | [A-z]\_U | AsciiToChar\_U |
| Variables de Entrada | lower\_lowerCase | 30 | No | in | No | in\_[a-Z] | in\_nombreUsuario |
| Variables | lowerCase | 30 | No | No | No | [a-Z] | nombreUsuario |
| Trigger | UPPER\_CamelCase\_UPPER | 50 | No | TR | Insert (I) Update (U) Delete (D) | TR\_[A-z]\_[I,U,D] | TR\_AuditoriaDatos |

**Importancias de las nomenclaturas al programar.**

Citando blog de página web [https://www.seas.es/blog/informatica/guia-del-desarrollador-nomenclatura-i/](https://www.seas.es/blog/informatica/guia-del-desarrollador-nomenclatura-i/)

Guía del desarrollador: Nomenclatura. I.

Con este post pretendo iniciar una serie de recomendaciones, no solo mías sino también de grandes gurús del mundo de la programación. Todos estos consejos aparecen recopilados en el excelente ** libro &quot;Clean code&quot;**  de Robert C. Martin. Así que, sin más dilación, abrimos el telón…

**Primera ley de la nomenclatura: Usa nombres con sentido.**

Cuando escribimos código estamos también contando una historia. Debemos tener claro que cada palabra cuenta, cada palabra debe aportar información y sentido. Establecer nombres de variables que no representen el valor que almacenan no tiene sentido.

**Primer corolario: Si una variable necesita comentario, el nombre es incorrecto.**

Debemos tener claro que &quot;no nos van a cobrar por cada letra que escribamos en el nombre de una variable&quot; y tampoco va a afectar al rendimiento de nuestra aplicación así que, establece nombres apropiados.
```java
int d; // tiempo transcurrido en días.

int elapsedTimeInDays;
```
Como se observa, la información es similar, pero gracias al nombre de la variable seremos conscientes en todo momento de lo que representa esa variable.

**Segundo corolario: Si un método no expresa su función, la nomenclatura es incorrecta.**

Cuanto mayor es el uso de una nomenclatura incorrecta, mucho más complejo es entender ese código.
```java
public List<int[]> getSome (int x, int y) {
    List<int]> ac = new ArrayList<int[]>(); 
    for (int i = 0; i < theList. Length; i++) { 
        for (int j = 0; < theList[i].length; j++) { 
            if (theList[i][j] == 0 && ((x == 1 - 1 || x == 1 + 1) && (y == ; - i |y == i + 1))) { 
                int[] c = new int [3]; 
                c[0] = i;
                c[1] = j;
                c[2] = theList[i][j];
                    ac.add (c);
            }
        }
    }
    return ac;
}
```
Una función o procedimiento escrito de esta manera plantea más incógnitas de las que resuelve, por lo tanto, deberemos revisarlo: ¿Qué es exactamente lo que hace? ¿A qué corresponde &quot;theList&quot; ? ¿Cuál es el motivo por el que se almacenan estos valores en esas posiciones?…

```java
public List<int[] > getAllowedCells(int row, int column) {
    List<int> allowedCells = new ArrayList int[]>(); 
    for (int i = 0; i < all Cells.length; i++) { 
        for (int j = 0; j < allCells(il.length; j++) { 
            if ( alicells[i][j] == EMPTY_VALUE && ((row =i - 1 || row = i + 1) && (y == j -1 Il y == j + 1)))) {
                cell = new int [3]; 
                cell[CELL_X] = i; 
                cell[CELL_Y] = j; 
                cell[VALUE] = al.Cells[i][j]; 
                allowedCells.add(cell);
            }
        }
    }
    return allowedCells;
}
```

Con un cambio en los nombres pero no en la lógica ya se observa una mejor. Usar variables en lugar de números enteros facilita la lectura del código y aporta mucha más información. Pero aún podríamos clarificar el código creando nuestras propias clases y métodos convirtiendo nuestro código en uno mucho más legible.

```java
public List<Cell> getAllowedCells (Cell origin) {
    List<Cell> allowedCells = new ArrayList<.0); 
    for (Cell cell : availableCell){
        if (cell.isEmpty() && cel.isNextTo(origin){
            allowedCells.add(cell); 
        }
    }   
    return allowedCells;
}
```

Con el desarrollo de la clase Cell y los métodos isEmpty(), que comprobará que la celda está vacía, y isNextTo(Cell cell), que comprobará que se encuentra en diagonal en un margen de una celda, simplificamos el código y lo acercamos al lenguaje natural.

## Herramientas

* [apexsql](https://solutioncenter.apexsql.com/rules-of-sql-formatting-sql-naming-conventions-and-capitalization-rules/) - Apex - Felipe Muñoz





## Licencia


Este proyecto está licenciado bajo la Licencia MIT - vea el [LICENSE.md](LICENSE.md) Para mas detalle
